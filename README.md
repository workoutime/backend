# Projeto workoutime
- Backend

# Tecnologias utilizadas

## Node.js
- Nodejs é uma plataforma de aplicação baseado em javascript com diversas
funcionalidades que facilitam o desenvolvimento de uma api.
- Os programas desenvolvidos com o nodejs serão compilados e interpretados pela
máquina virtual v8, a mesma que o Google utiliza para executar javaScript no
Chrome.
- Node.js é uma tecnologia assíncrona. Ou seja, cada requisição não bloqueia
seu processoo. Dessa forma é possível atender um grande volume de requisições.
- Utilizaremos o node para criar nosso backend e nossa API REST.

# TypeScript
- Iremos utilizar o typeScript, pois ele nos fornece algumas facilidades em
relação ao desenvolvimento:
  - Intellisense
  - Facilidade de entendimento do código pela equipe, por seguir um padrão.
  - O mercado está usando cada vez mais o typeScript.
- O TypeScript não substitui o JavaScript. Na verdade aquele foi feito baseado
neste.
- O código feito em typeScript será convertido para JS quando for executado.
- O Typescript possibilita-nos a criação de tipos de acordo com o nosso
modelo de nogócio.

# Express.js
- Framework minimalista e rápido para Node.js.
- Perfeito para a criação de APIs REST.
- Nos possibilita criar rotas para serem acessadas através dos métodos HTTP
bem como middlewares para controles necessários.

# Jest
- Biblioteca utilizada para execução dos testes.
- Como essa é uma aplicação de demonstração, não cobri todo o código com os
testes.
- Criei alguns testes principais para cobrir o crud das entidades do negócio.

# JWT
- A configuração de json web token está pronta. Contudo não cheguei a adicionar
no login, para que o token fosse retornado ao frotend.
- Retornando o token, podemos definir quais rotas o usuário somente poderá
acessar com posse de um token válido.

# Banco de dados

## Postgres
- O banco de dados utilizado foi o postgres, mas qualquer outro pode ser
utilizado de acordo com a arquitetura montada.

## Typeorm
- Utilizei essa biblioteca de ORM que nos permite escrever queries em formato
javascript.
- Permite uma fácil configuração para conexão com o banco de dados.
- Cria um baixo acoplamento em relação ao SGDB utilizado.
Link: https://typeorm.io/

## Models
- Na pasta ficam os modelos que são as entidades do mundo real que serão
armazenadas em banco de dados. O typeorm nos permite utilizar o conceito de
annotations para definir o tipo de cada atributo, bem como as restrições
de integridade.
- As migrations devem ser criadas com base nos modelos definidos.

## Typeorm Migrations
- As migrations são utilizadas para realizar um controle de versão do nosso
banco de dados.
- Podemos fazer upgrade e rollbacks com facilidade.
- Histórico de banco de dados.
- A ordem dos arquivos criados dentro da pasta migrations, será a ordem que
elas serão executadas no Banco de Dados.
- Comando para criar uma migration:
```
yarn typeorm migration:create -n nomeDescritivo
```
- Feito isso, é precisso executar a migration para que a query seja executada:
```
yarn typeorm migration:run
```

## ormconfig.json
- Toda a configuração com o banco de dados fica nesse arquivo.
- Dessa forma criamos um arquivo index.ts dentro de uma pasta database que
irá tratar de fazer a conexão com base no arquivo ormconfig.json.
- O arquivo disponibilizado permite a conexão com o banco de dados local.
- Há um outro projeto junto do backend com um arquivo docker-compose que
permite a criação do banco de dados local.
https://gitlab.com/workoutime/postgre-local

# Estrutura do projeto

# API
- O objetivo principal do projeto é criar um backend para o frontend do
workoutime.
- As rotas serão criadas através da lib express.
- Cada api está com a nomenclatura mais descritiva o possível, para isolar o máximo cada funcionalidade.

# Rotas
- O ponto de entrada para a comunicação do frontend com o backend é a rota.
- Essa somente recebe uma requisição e retorna uma resposta.
- Toda lógica de negócio será executada fora dessa função.

# UseCases
- Segui o padrão de package by features.
- Ou seja, a estrutura do código está organizada por funcionalidade.
- Para cada caso de uso criamos um conjunto de classes:

### Controller
- Somente trata o request vindo da rota e o response a ser enviado de volta.

### UseCase
- Será responsável por toda a regra de negócio daquele caso de uso.
- Onde será realmente implementada a lógica para atender a requisição do
usuário.

# Data Transfer Object - DTO
- Irão representar uma estrutura do mundo real ou do nosso mundo negocial.
- Interfaces que representam os dados a serem transferidos para cada caso de uso.

# Repository
- Interfaces para comunicações com a base de dados.
Aqui seguimos o princípio de Liskov substitution principle:
  - Independente de onde os dados estiverem armazenados, nós poderemos alterar
  a qualquer momento o acesso, sem que o caso de uso que esteja implementando
  nossa interface de Repositório sinta essas mudanças.

# Resumo do fluxo de um DTO na arquiteura da aplicação
1 - Rotas para abrir a comunicação com o frontend.
2 - Controller para tratar as requisições e enviar uma resposta.
3 - UseCase para realizar as regras de negócio.
4 - Repositorio para buscar os dados solicitados pela regra de negócio.
5 - Os dados passam novamente pelo UseCase.
6 - São retornados pelo Cotroller para a rota.
7 - A rota envia os dados prontos para a tela.

# Executando os testes
- Os testes estão dentro da pasta __tests__
- Execute o comando: yarn test, para rodar os testes.

# Qualidade de código

## Padronização de commits - Convetional Commits

### commitlint
- Vai proibir commits que não sigam o padrão determinado pelo convetional commit.
- https://github.com/conventional-changelog/commitlint

### commitizen
- Vai criar uma interface visual na linha de comando para facilitar que o
desenvolvedor consiga seguir o convetional commit, através de um template.
- Vai criar um padrão que segue o config conventional da comunidade do angular.
- Nada impede que o time crie seus próprios padrões e troque o caminho da pasta
dentro do package.json.
- Adiciona-se um script dentro do package.json para que, quando executado com
o git, o template seja iniciado. ("commit": "git-cz")
- Adição de mais uma configuração no husky para que o dev possar iniciar o
template ao digitar git commit.
```
"prepare-commit-msg": "exec < /dev/tty && git cz --hook || true"
```
- https://github.com/commitizen/cz-cli

## EditorConfig
-  É um plugin que auxilia na padronização da configuração para vários
desenvolvedores trabalhando em um mesmo projeto, mas em diferentes editores de
código ou IDE's.
https://editorconfig.org/

## Eslint
- Ferramenta que nos auxilia no momento de padronizarmos o nosso projeto.
- Com ela conseguimos automatizar os padrões de códigos do nosso projeto.
https://eslint.org/docs/rules/

## Prettier
- O Prettier é mais uma ferramenta que vamos utilizar para ajudar na
padronização de código, ele consiste em várias configurações que são feitas
para que o código seja formatado para seguir um padrão.
https://prettier.io/docs/en/options.html

## husky
- Vai permitir a configuração dos HOOKS do git.
- Dessa forma podemos definir as ações que devem ser feitas antes de um commit,
por exemplo.



