/**
 * Como é uma aplicação de teste, vou deixar o hash utilizado para criar o token exposto.
 * Criado com bcrypt generator.
 */
export default {
  jwt:  {
    secret: '$2y$08$zXJF.DIziROykZ/o1g1wPOwD7tSW.bFTkQNrmmAIBi87Hi4ny2bze',
    expiresIn: '1d'
  }
}
