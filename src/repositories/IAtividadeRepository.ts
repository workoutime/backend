import AtividadeDTO from "../DTO/AtividadeDTO";
import Atividade from "../models/Atividade";

export default interface IAtividadeRepository {

  /**
   * @param AtividadeDTO
   */
  criar(atividadeDTO: AtividadeDTO):Promise<Atividade>;

  buscarAtividadesPorUsuario(id: string):Promise<Atividade[]>;

  remover(id: string):Promise<boolean>;
}
