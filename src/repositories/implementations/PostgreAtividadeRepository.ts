import { EntityRepository, getConnection, getRepository, Repository } from "typeorm";
import AtividadeDTO from "../../DTO/AtividadeDTO";
import Atividade from "../../models/Atividade";
import IAtividadeRepository from "../IAtividadeRepository";

@EntityRepository(Atividade)
export class PostgreAtividadeRepository extends Repository<Atividade> implements IAtividadeRepository{

  async criar(atividadeDTO: AtividadeDTO): Promise<Atividade> {
    const atividadeCriada = getRepository(Atividade).create(atividadeDTO);
    await getRepository(Atividade).save(atividadeCriada);
    return atividadeCriada;
  }

  async buscarAtividadesPorUsuario(id: string): Promise<Atividade[]> {
    let atividades = await getRepository(Atividade).find({
      where: {usuario_id: id}
    })
    return atividades;
  }

  async remover(id: string): Promise<boolean> {
    try {
      getConnection()
      .createQueryBuilder()
      .delete()
      .from(Atividade)
      .where("id = :id", { id: id })
      .execute();
      return true;
    } catch(err){
      return false;
    }
  }

}

