import { EntityRepository, getRepository, Repository } from "typeorm";
import IUsuarioSessionDTO from "../../DTO/IUsuarioSessionDTO";
import UsuarioDTO from "../../DTO/UsuarioDTO";
import Usuario from "../../models/Usuario";
import IUsuarioRepository from "../IUsuarioRepository";

@EntityRepository(Usuario)
export class PostgreUsuarioRepository extends Repository<Usuario> implements IUsuarioRepository{

  async criar(usuarioDTO: UsuarioDTO): Promise<UsuarioDTO> {
    const usuarioCriado = getRepository(Usuario).create(usuarioDTO);
    await getRepository(Usuario).save(usuarioCriado);
    return usuarioCriado;
  }

  async buscaUsuarioPorId(id?: string): Promise<Usuario | undefined> {
    const usuarioEncontrado = await getRepository(Usuario).findOne({
      where: { id }
    })

    return usuarioEncontrado;
  }

  async buscaUsuarioPorEmail(email?: string): Promise<Usuario | undefined> {
    const usuarioEncontrado = await getRepository(Usuario).findOne({
      where: { email }
    })

    return usuarioEncontrado;
  }

  async remover(usuario: Usuario): Promise<boolean> {
    try {
      await getRepository(Usuario).remove(usuario);
      return true;
    } catch(err){
      return false;
    }
  }

}

