import IUsuarioSessionDTO from "../DTO/IUsuarioSessionDTO";
import UsuarioDTO from "../DTO/UsuarioDTO";
import Usuario from "../models/Usuario";

export default interface IUsuarioRepository {

  /**
   * @param Usuario
   */
  criar(usuarioDTO: UsuarioDTO):Promise<UsuarioDTO>;

  buscaUsuarioPorEmail(email?: string):Promise<Usuario | undefined>;

  buscaUsuarioPorId(email?: string):Promise<UsuarioDTO | undefined>;

  remover(usuario: Usuario):Promise<boolean>;
}
