declare namespace Express {
  /**
   * Faz um anexo na interface já existente
   */
  export interface Request {
    usuario: {
      id: string;
    };
  }
}
