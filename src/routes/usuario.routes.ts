import { Router } from 'express';
import { criaUsuarioController } from '../useCases/Usuarios/CriaUsuario'
import { buscaUsuariosController } from '../useCases/Usuarios/BuscaUsuarios'

const usuariosRouter = Router();

usuariosRouter.post('/', async (request, response) => {
    try {
        const user = await criaUsuarioController.handle(request, response);
        return user;
    } catch (err) {
        return response.status(400).json({ error: err.message });
    }
});

usuariosRouter.get('/', async (request, response) => {
  try {
      const user = await buscaUsuariosController.handle(request, response);
      return user;
  } catch (err) {
      return response.status(400).json({ error: err.message });
  }
});

export default usuariosRouter;
