import { Router } from 'express';
import { criarSessionController } from '../useCases/Session';

const sessionRouter = Router();

sessionRouter.post('/', async (request, response) => {
  try {
    const usuarioEToken = await criarSessionController.handle(request, response);
    return usuarioEToken;
} catch (err) {
    return response.status(400).json({ error: err.message });
}
});

export default sessionRouter;
