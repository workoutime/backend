import { Router } from 'express';
import atividadesRouter from './atividade.routes';
import sessionRouter from './sessions.routes';
import usuariosRouter from './usuario.routes';

const routes = Router();

routes.use('/usuario', usuariosRouter);
routes.use('/atividade', atividadesRouter);
routes.use('/session', sessionRouter);


export default routes;
