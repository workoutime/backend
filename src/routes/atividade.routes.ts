import { Router } from 'express';
import verificaAutenticidade from '../middlewares/verificaAutenticidade';
import { buscarAtividadesPorUsuarioController } from '../useCases/Atividades/BuscarAtividadesPorUsuario';
import { criarAtividadeController } from '../useCases/Atividades/CriarAtividade'
import { removerAtividadeController } from '../useCases/Atividades/RemoverAtividade';

const atividadesRouter = Router();

atividadesRouter.post('/', async (request, response) => {
    try {
      console.log("CHEGA AQUI")
        const atividade = await criarAtividadeController.handle(request, response);
        return atividade;
    } catch (err) {
        return response.status(400).json({ error: err.message });
    }
});

atividadesRouter.get('/', async (request, response) => {
  try {
      const atividade = await buscarAtividadesPorUsuarioController.handle(request, response);
      return atividade;
  } catch (err) {
      return response.status(400).json({ error: err.message });
  }
});

atividadesRouter.delete('/', async (request, response) => {
  try {
      const atividade = await removerAtividadeController.handle(request, response);
      return atividade;
  } catch (err) {
      return response.status(400).json({ error: err.message });
  }
});

export default atividadesRouter;
