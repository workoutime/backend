import {Request, Response} from 'express';
import SessionDTO from '../../DTO/SessionDTO';
import { CriarSessionUseCase } from './CriarSessionUseCase';


export class CriarSessionController {
  constructor(
    private criarSessionUseCase: CriarSessionUseCase
  ) {}
  async handle(request: Request, response: Response): Promise<Response>{
    try {
      console.log("AQUI")
      console.log(request.body)
      const { email, senha }: any = request.body;
      const sessionDTO = new SessionDTO({email, senha});
      let usuarioEncontrado = await this.criarSessionUseCase.execute(sessionDTO);
      console.log(usuarioEncontrado)
      response.setHeader('Access-Control-Allow-Origin', '*');
      return response.status(201).json(usuarioEncontrado);
    }catch(err){
      return response.status(400).json({
        message: "Usuário ou senha inválidos"
      })
    }
  }
}
