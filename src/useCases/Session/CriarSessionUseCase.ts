import {compare} from 'bcryptjs';
import SessionDTO from '../../DTO/SessionDTO';
import IUsuarioRepository from '../../repositories/IUsuarioRepository';
import { sign } from 'jsonwebtoken';
import authConfig from '../../config/auth';

export class CriarSessionUseCase {

  constructor(
    private usuarioRepository: IUsuarioRepository
  ){}

  async execute(sessionDTO: SessionDTO){

    const usuarioEncontrado = await this.usuarioRepository.buscaUsuarioPorEmail(sessionDTO.email)

    if(!usuarioEncontrado){
      throw new Error("Usuário ou senha inválidos");
    }

    const isSenhaCorreta = await compare(sessionDTO.senha, usuarioEncontrado.senha as string)

    if(!isSenhaCorreta){
      throw new Error("Usuário ou senha inválidos");
    }

    //delete usuarioEncontrado.senha;

    const {secret, expiresIn} = authConfig.jwt;

    const token = sign({}, secret, {
      subject: usuarioEncontrado.id,
      expiresIn: expiresIn
    })

    return {usuarioEncontrado, token };
  }
}
