import { PostgreUsuarioRepository } from "../../repositories/implementations/PostgreUsuarioRepository";
import { CriarSessionController } from "./CriarSessionController";
import { CriarSessionUseCase } from "./CriarSessionUseCase";

const postgreUsuarioRepository = new PostgreUsuarioRepository();

const criarSessionUseCase = new CriarSessionUseCase(
  postgreUsuarioRepository
);

const criarSessionController = new CriarSessionController(
  criarSessionUseCase
);

export {criarSessionUseCase, criarSessionController};
