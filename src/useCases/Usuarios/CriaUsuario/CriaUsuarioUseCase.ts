import UsuarioDTO from "../../../DTO/UsuarioDTO";
import IUsuarioRepository from "../../../repositories/IUsuarioRepository";
import {hash} from 'bcryptjs';

export class CriaUsuarioUseCase {

  constructor(
    private usuarioRepository: IUsuarioRepository
  ){}

  async execute(usuarioDTO: UsuarioDTO){

      const usuarioEncontrado = await this.usuarioRepository.buscaUsuarioPorEmail(usuarioDTO.email);

      if(usuarioEncontrado){
        throw new Error("Usuário já cadastrado!")
      }

      if(usuarioDTO.senha){
        const hashDaSenha = await hash(usuarioDTO.senha, 8);
        usuarioDTO.senha = hashDaSenha;
      }

      const usuarioCadastrado = await this.usuarioRepository.criar(usuarioDTO);

      delete usuarioCadastrado.senha;

      return usuarioCadastrado;

  }
}
