import { PostgreUsuarioRepository } from "../../../repositories/implementations/PostgreUsuarioRepository";
import { CriaUsuarioController } from "./CriaUsuarioController";
import { CriaUsuarioUseCase } from "./CriaUsuarioUseCase";

const postgreUsuarioRepository = new PostgreUsuarioRepository();

const criaUsuarioUseCase = new CriaUsuarioUseCase(
  postgreUsuarioRepository
);

const criaUsuarioController = new CriaUsuarioController(
  criaUsuarioUseCase
);

export {criaUsuarioUseCase, criaUsuarioController};
