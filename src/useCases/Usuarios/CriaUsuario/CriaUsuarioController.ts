import {Request, Response} from 'express';
import UsuarioDTO from '../../../DTO/UsuarioDTO';
import { CriaUsuarioUseCase } from './CriaUsuarioUseCase';

export class CriaUsuarioController {
  constructor(
    private criaUsuarioUseCase: CriaUsuarioUseCase
  ) {}
  async handle(request: Request, response: Response): Promise<Response>{
    try {
      const {nome, email, senha}: any = request.body;
      const usuarioDTO = new UsuarioDTO({nome, email, senha});
      let usuarioEncontrado = await this.criaUsuarioUseCase.execute(usuarioDTO);
      response.setHeader('Access-Control-Allow-Origin', '*');
      return response.status(201).json(usuarioEncontrado);
    }catch(err){
      return response.status(400).json({
        message: err.message
      })
    }
  }
}
