import UsuarioDTO from "../../../DTO/UsuarioDTO";
import IUsuarioRepository from "../../../repositories/IUsuarioRepository";
import {buscaUsuariosUseCase} from '../BuscaUsuarios';

export class RemoveUsuariosUseCase {

  constructor(
    private usuarioRepository: IUsuarioRepository
  ){}

  async execute(usuarioDTO: UsuarioDTO): Promise<boolean>{

    const usuarioEncontrado = await buscaUsuariosUseCase.execute(usuarioDTO);

    let isUsuarioRemovido = true;
    if(usuarioEncontrado){
      await this.usuarioRepository.remover(usuarioEncontrado);
    }else{
      isUsuarioRemovido = false
    }

    return isUsuarioRemovido;
  }
}
