import {Request, Response} from 'express';
import UsuarioDTO from '../../../DTO/UsuarioDTO';
import { RemoveUsuariosUseCase } from './RemoveUsuariosUseCase';

export class RemoveUsuariosController {
  constructor(
    private removeUsuariosUseCase: RemoveUsuariosUseCase
  ) {}
  async handle(request: Request, response: Response): Promise<Response>{
    try {
      const {nome, email, senha}: any = request.body;
      const usuarioDTO = new UsuarioDTO({nome, email, senha});
      let usuarioRemovido = await this.removeUsuariosUseCase.execute(usuarioDTO);
      response.setHeader('Access-Control-Allow-Origin', '*');
      response.setHeader('Access-Control-Allow-Origin', '*')
      if(usuarioRemovido){
        return response.status(201).json({message: "Usuario removido com sucesso"});
      }
      else{
        return response.status(400).json({
          message: "Erro ao remover usuário."
        });
      }
    }catch(err){
      return response.status(400).json({
        message: err.message
      })
    }
  }
}
