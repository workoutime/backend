import { PostgreUsuarioRepository } from "../../../repositories/implementations/PostgreUsuarioRepository";
import { RemoveUsuariosController } from "./RemoveUsuariosController";
import { RemoveUsuariosUseCase } from "./RemoveUsuariosUseCase";

const postgreUsuarioRepository = new PostgreUsuarioRepository();

const removeUsuariosUseCase = new RemoveUsuariosUseCase(
  postgreUsuarioRepository
);

const removeUsuariosController = new RemoveUsuariosController(
  removeUsuariosUseCase
);

export {removeUsuariosUseCase, removeUsuariosController};
