import IUsuarioSessionDTO from "../../../DTO/IUsuarioSessionDTO";
import UsuarioDTO from "../../../DTO/UsuarioDTO";
import Usuario from "../../../models/Usuario";
import IUsuarioRepository from "../../../repositories/IUsuarioRepository";


export class BuscaUsuariosUseCase {

  constructor(
    private usuarioRepository: IUsuarioRepository
  ){}

  async execute(usuarioDTO: UsuarioDTO): Promise<Usuario | undefined>{

    const usuarioEncontrado = this.usuarioRepository.buscaUsuarioPorEmail(usuarioDTO.email);

    if(!usuarioEncontrado){
      throw new Error("Usuário não cadastrado.");
    }

    return usuarioEncontrado;
  }
}
