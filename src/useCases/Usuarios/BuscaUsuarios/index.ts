import { PostgreUsuarioRepository } from "../../../repositories/implementations/PostgreUsuarioRepository";
import { BuscaUsuariosController } from "./BuscaUsuariosController";
import { BuscaUsuariosUseCase } from "./BuscaUsuariosUseCase";

const postgreUsuarioRepository = new PostgreUsuarioRepository();

const buscaUsuariosUseCase = new BuscaUsuariosUseCase(
  postgreUsuarioRepository
);

const buscaUsuariosController = new BuscaUsuariosController(
  buscaUsuariosUseCase
);

export {buscaUsuariosUseCase, buscaUsuariosController};
