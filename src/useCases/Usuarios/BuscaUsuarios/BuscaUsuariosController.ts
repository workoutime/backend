import {Request, Response} from 'express';
import UsuarioDTO from '../../../DTO/UsuarioDTO';
import { BuscaUsuariosUseCase } from './BuscaUsuariosUseCase';

export class BuscaUsuariosController {
  constructor(
    private buscaUsuariosUseCase: BuscaUsuariosUseCase
  ) {}
  async handle(request: Request, response: Response): Promise<Response>{
    try {
      const {nome, email, senha}: any = request.body;
      const usuarioDTO = new UsuarioDTO({nome, email, senha});
      let usuarioEncontrado = await this.buscaUsuariosUseCase.execute(usuarioDTO);
      response.setHeader('Access-Control-Allow-Origin', '*');
      if(usuarioEncontrado){
        return response.status(201).json(usuarioEncontrado);
      }
      else{
        return response.status(400).json({
          message: "Usuário não cadastrado."
        });
      }
    }catch(err){
      return response.status(400).json({
        message: err.message
      })
    }
  }
}
