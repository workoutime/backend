import AtividadeDTO from "../../../DTO/AtividadeDTO";
import IAtividadeRepository from "../../../repositories/IAtividadeRepository";

export class RemoverAtividadeUseCase {

  constructor(
    private atividadeRepository: IAtividadeRepository
  ){}

  async execute(id: string):Promise<any>{
    const atividades = await this.atividadeRepository.remover(id);
    return atividades;
}

}
