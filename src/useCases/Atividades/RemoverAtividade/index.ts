import { PostgreAtividadeRepository } from "../../../repositories/implementations/PostgreAtividadeRepository";
import { RemoverAtividadeController } from "./RemoverAtividadeController";
import { RemoverAtividadeUseCase } from "./RemoverAtividadeUseCase";

const postgreAtividadeRepository = new PostgreAtividadeRepository();

const removerAtividadeUseCase = new RemoverAtividadeUseCase(
  postgreAtividadeRepository
);

const removerAtividadeController = new RemoverAtividadeController(
  removerAtividadeUseCase
);

export {removerAtividadeUseCase, removerAtividadeController};
