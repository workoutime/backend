import {Request, Response} from 'express';
import { RemoverAtividadeUseCase } from './RemoverAtividadeUseCase';


export class RemoverAtividadeController {
  constructor(
    private removerAtividadeUseCase: RemoverAtividadeUseCase
  ) {}
  async handle(request: Request, response: Response): Promise<Response>{
    try {
      const {id}: any = request.query;
      let isAtividadeRemovida = await this.removerAtividadeUseCase.execute(id);
      response.setHeader('Access-Control-Allow-Origin', '*');
      return response.status(201).json(isAtividadeRemovida);
    }catch(err){
      return response.status(400).json({
        message: err.message
      })
    }
  }
}
