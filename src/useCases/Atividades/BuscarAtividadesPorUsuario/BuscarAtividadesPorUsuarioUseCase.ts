import Atividade from "../../../models/Atividade";
import IAtividadeRepository from "../../../repositories/IAtividadeRepository";

export class BuscarAtividadesPorUsuarioUseCase {

  constructor(
    private atividadeRepository: IAtividadeRepository
  ){}

  async execute(id: string):Promise<Atividade[]>{
      const atividades = await this.atividadeRepository.buscarAtividadesPorUsuario(id);
      return atividades;
  }
}
