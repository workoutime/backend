import { PostgreAtividadeRepository } from "../../../repositories/implementations/PostgreAtividadeRepository";
import { BuscarAtividadesPorUsuarioController } from "./BuscarAtividadesPorUsuarioController";
import { BuscarAtividadesPorUsuarioUseCase } from "./BuscarAtividadesPorUsuarioUseCase";

const postgreAtividadeRepository = new PostgreAtividadeRepository();

const buscarAtividadesPorUsuarioUseCase = new BuscarAtividadesPorUsuarioUseCase(
  postgreAtividadeRepository
);

const buscarAtividadesPorUsuarioController = new BuscarAtividadesPorUsuarioController(
  buscarAtividadesPorUsuarioUseCase
);

export {buscarAtividadesPorUsuarioUseCase, buscarAtividadesPorUsuarioController};
