import {Request, Response} from 'express';
import { BuscarAtividadesPorUsuarioUseCase } from './BuscarAtividadesPorUsuarioUseCase';

export class BuscarAtividadesPorUsuarioController {
  constructor(
    private buscarAtividadesPorUsuarioUseCase: BuscarAtividadesPorUsuarioUseCase
  ) {}
  async handle(request: Request, response: Response): Promise<Response>{
    try {
      const {id}: any = request.query;

      let atividadesDoUsuario = await this.buscarAtividadesPorUsuarioUseCase.execute(id);
      response.setHeader('Access-Control-Allow-Origin', '*');
      return response.status(201).json(atividadesDoUsuario);
    }catch(err){
      return response.status(400).json({
        message: err.message
      })
    }
  }
}
