import {Request, Response} from 'express';
import AtividadeDTO from '../../../DTO/AtividadeDTO';
import { CriarAtividadeUseCase } from './CriarAtividadeUseCase';
import parseISO from 'date-fns/parseISO';

export class CriarAtividadeController {
  constructor(
    private criarAtividadeUseCase: CriarAtividadeUseCase
  ) {}
  async handle(request: Request, response: Response): Promise<Response>{
    try {
      const {usuario_id, data, tipo_atividade, tempo_gasto}: any = request.body;
      const dataFormatada = parseISO(data);
      const atividadeDTO = new AtividadeDTO({usuario_id, data: dataFormatada, tipo_atividade, tempo_gasto});
      let atividadeCriada = await this.criarAtividadeUseCase.execute(atividadeDTO);
      response.setHeader('Access-Control-Allow-Origin', '*');
      return response.status(201).json(atividadeCriada);
    }catch(err){
      return response.status(400).json({
        message: err.message
      })
    }
  }
}
