import AtividadeDTO from "../../../DTO/AtividadeDTO";
import IAtividadeRepository from "../../../repositories/IAtividadeRepository";

export class CriarAtividadeUseCase {

  constructor(
    private atividadeRepository: IAtividadeRepository
  ){}

  async execute(atividadeDTO: AtividadeDTO){
      const atividade = await this.atividadeRepository.criar(atividadeDTO);
      console.log(atividade)

      return atividade;
  }
}
