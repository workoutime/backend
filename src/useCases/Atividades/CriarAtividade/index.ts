import { PostgreAtividadeRepository } from "../../../repositories/implementations/PostgreAtividadeRepository";
import { CriarAtividadeController } from "./CriarAtividadeController";
import { CriarAtividadeUseCase } from "./CriarAtividadeUseCase";

const postgreAtividadeRepository = new PostgreAtividadeRepository();

const criarAtividadeUseCase = new CriarAtividadeUseCase(
  postgreAtividadeRepository
);

const criarAtividadeController = new CriarAtividadeController(
  criarAtividadeUseCase
);

export {criarAtividadeUseCase, criarAtividadeController};
