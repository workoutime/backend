import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export default class CriarTabelaDeAtividades1603928917490 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.createTable(
        new Table({
          name: "atividades",
          columns: [
            {
              name: 'id',
              type: 'uuid',
              isPrimary: true,
              generationStrategy: 'uuid',
              default: 'uuid_generate_v4()',
            },
            {
              name: 'usuario_id',
              type: 'uuid',
              isNullable: false
            },
            {
              name: 'data',
              type: 'timestamp with time zone',
              isNullable: false
            },
            {
              name: 'tipo_atividade',
              type: 'varchar',
              isNullable: false
            },
            {
              name: 'tempo_gasto',
              type: 'int',
              isNullable: false
            },
            {
              name: 'created_at',
              type: 'timestamp with time zone',
              default: 'now()',
            },
            {
              name: 'updated_at',
              type: 'timestamp with time zone',
              default: 'now()',
            }
          ]
        })
      );

      await queryRunner.createForeignKey(
        'atividades',
        new TableForeignKey({
            name: 'fk_atividade_usuario',
            columnNames: ['usuario_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'usuarios',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }),
      );

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropForeignKey(
        'atividades',
        'fk_atividade_usuario',
      );

      await queryRunner.dropTable('atividades');
    }

}
