import { Request, Response, NextFunction } from "express"
import {verify} from 'jsonwebtoken';
import authConfig from '../config/auth';
import IJwtDTO from "../DTO/IJwtDTO";

export default function verificaAutenticidade(request: Request, response: Response, next: NextFunction): void{

  const authHeader = request.headers.authorization;

  if(!authHeader){
    throw new Error('Requisição sem token JWT');
  }

  /**
   * Formato: Bearer token
   */
  const [,token] = authHeader.split(' ');

  try{
    const decoded = verify(token, authConfig.jwt.secret);

    const { sub } = decoded as IJwtDTO;

    return next();
  }catch {
    throw new Error("Token JWT inválido.")
  }

}
