export default interface IJwtDTO {
  iat: number;
  exp: number;
  sub: string;
}
