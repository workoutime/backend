export default interface IUsuarioSessionDTO {
  id: string;
  nome: string;
  email: string;
  senha?:string;
}
