export default class UsuarioDTO {
  nome?: string;
  email?: string;
  senha?:string;

  constructor({nome, email, senha}) {
    this.nome = nome;
    this.email = email;
    this.senha = senha;
  }
}
