import { createConnection, getConnection, getRepository } from 'typeorm';
import SessionDTO from '../../../DTO/SessionDTO';
import UsuarioDTO from '../../../DTO/UsuarioDTO';
import { criarSessionController, criarSessionUseCase } from '../../../useCases/Session';
import { buscaUsuariosUseCase } from '../../../useCases/Usuarios/BuscaUsuarios';
import { criaUsuarioUseCase } from '../../../useCases/Usuarios/CriaUsuario';
import { removeUsuariosUseCase } from '../../../useCases/Usuarios/RemoveUsuarios';


describe('testar o crud de um usuario', () => {

  beforeAll(async () => {
    await createConnection();
  });;

  afterAll(async () => {
    await getConnection().close();
  });;

  test('deve inserir um usuario', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteSession", email: "teste.session@gmail.com", senha: "123"});
    const usuario = await criaUsuarioUseCase.execute(usuarioDTO);
    expect(usuario);
  });

  test('deve realizar login com sucesso', async () => {
    const sessionDTO = new SessionDTO({ email: "teste.session@gmail.com", senha: "123"});
    const response = await criarSessionUseCase.execute(sessionDTO);
    expect(response.usuarioEncontrado.email).toEqual("teste.session@gmail.com");
  });

  test('deve realizar login com sucesso', async () => {
    const sessionDTO = new SessionDTO({ email: "teste@gmail1.com", senha: "123"});
    try{
      await criarSessionUseCase.execute(sessionDTO);
      expect(false);
    }catch(err){
      console.log(err);
      expect(true);
    }
  });

  test('deve buscar um usuario de acordo com o email', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteSession", email: "teste.session@gmail.com", senha: "123"});

    const isUsuarioRemovido = await removeUsuariosUseCase.execute(usuarioDTO);

    expect(isUsuarioRemovido);
  });
});
