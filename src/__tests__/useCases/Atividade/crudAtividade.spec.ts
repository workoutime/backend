import { createConnection, getConnection, getRepository } from 'typeorm';
import AtividadeDTO from '../../../DTO/AtividadeDTO';
import UsuarioDTO from '../../../DTO/UsuarioDTO';
import Atividade from '../../../models/Atividade';
import { buscarAtividadesPorUsuarioUseCase } from '../../../useCases/Atividades/BuscarAtividadesPorUsuario';
import { criarAtividadeUseCase } from '../../../useCases/Atividades/CriarAtividade';
import { removerAtividadeUseCase } from '../../../useCases/Atividades/RemoverAtividade';
import { buscaUsuariosUseCase } from '../../../useCases/Usuarios/BuscaUsuarios';
import { criaUsuarioUseCase } from '../../../useCases/Usuarios/CriaUsuario';
import { removeUsuariosUseCase } from '../../../useCases/Usuarios/RemoveUsuarios';


describe('testar o crud de atividades', () => {

  beforeAll(async () => {
    await createConnection();
  });;

  afterAll(async () => {
    await getConnection().close();
  });

  test('deve inserir um usuario', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteAtividade", email: "teste.atividade@gmail.com", senha: "123"});
    const usuario = await criaUsuarioUseCase.execute(usuarioDTO);
    expect(usuario);
  });

  test('deve inserir algumas atividades para um determinado usuário', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteAtividade", email: "teste.atividade@gmail.com", senha: "123"});
    const usuarioEncontrado = await buscaUsuariosUseCase.execute(usuarioDTO);
    const atividadeDTO1 = new AtividadeDTO({ usuario_id: usuarioEncontrado?.id, data: new Date(), tipo_atividade: "musculação", tempo_gasto: 20});
    const atividadeDTO2 = new AtividadeDTO({ usuario_id: usuarioEncontrado?.id, data: new Date(), tipo_atividade: "corrida", tempo_gasto: 10});
    const atividadeDTO3 = new AtividadeDTO({ usuario_id: usuarioEncontrado?.id, data: new Date(), tipo_atividade: "futvôlei", tempo_gasto: 30});
    const atividade1 = await criarAtividadeUseCase.execute(atividadeDTO1);
    const atividade2 = await criarAtividadeUseCase.execute(atividadeDTO2);
    const atividade3 = await criarAtividadeUseCase.execute(atividadeDTO3);
    expect(atividade1);
    expect(atividade2);
    expect(atividade3);
  });

  test('deve buscar as atividades do usuario', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteAtividade", email: "teste.atividade@gmail.com", senha: "123"});
    const usuarioEncontrado = await buscaUsuariosUseCase.execute(usuarioDTO);
    let atividadeEncontradas: Atividade[];
    if(usuarioEncontrado){
      atividadeEncontradas = await buscarAtividadesPorUsuarioUseCase.execute(usuarioEncontrado?.id);
    }else{
      atividadeEncontradas = [];
    }
    expect(atividadeEncontradas.length).toEqual(3);
  });

  test('deve remover uma atividade do usuario', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteAtividade", email: "teste.atividade@gmail.com", senha: "123"});
    const usuarioEncontrado = await buscaUsuariosUseCase.execute(usuarioDTO);
    let atividadeEncontradas: Atividade[];
    if(usuarioEncontrado){
      atividadeEncontradas = await buscarAtividadesPorUsuarioUseCase.execute(usuarioEncontrado?.id);
    }else{
      atividadeEncontradas = [];
    }
    let isRemovida = removerAtividadeUseCase.execute(atividadeEncontradas[0].id);
    expect(isRemovida);
  });

  test('deve remover um usuario', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteAtividade", email: "teste.atividade@gmail.com", senha: "123"});

    const isUsuarioRemovido = await removeUsuariosUseCase.execute(usuarioDTO);

    expect(isUsuarioRemovido);
  });

});
