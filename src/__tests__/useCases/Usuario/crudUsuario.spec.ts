import { createConnection, getConnection, getRepository } from 'typeorm';
import UsuarioDTO from '../../../DTO/UsuarioDTO';
import { buscaUsuariosUseCase } from '../../../useCases/Usuarios/BuscaUsuarios';
import { criaUsuarioUseCase } from '../../../useCases/Usuarios/CriaUsuario';
import { removeUsuariosUseCase } from '../../../useCases/Usuarios/RemoveUsuarios';


describe('testar o crud de um usuario', () => {

  beforeAll(async () => {
    await createConnection();
  });;

  afterAll(async () => {
    await getConnection().close();
  });;


  test('deve inserir um usuario', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteUsuario", email: "teste.usuario@gmail.com", senha: "123"});
    const usuario = await criaUsuarioUseCase.execute(usuarioDTO);
    expect(usuario);
  });

  test('deve tentar inserir um usuario repetido', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteUsuario", email: "teste.usuario@gmail.com", senha: "123"});
    try{
      await criaUsuarioUseCase.execute(usuarioDTO);
      expect(false);
    }catch(err){
      expect(true);
    }
  });

  test('deve buscar um usuario de acordo com o email', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteUsuario", email: "teste.usuario@gmail.com", senha: "123"});
    const usuarioEncontrado = await buscaUsuariosUseCase.execute(usuarioDTO);
    expect(usuarioEncontrado?.email).toEqual('teste.usuario@gmail.com');
  });

  test('deve buscar um usuario que não existe', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteUsuario", email: "teste1@gmail.com", senha: "123"});
    const usuarioEncontrado = await buscaUsuariosUseCase.execute(usuarioDTO);
    expect(usuarioEncontrado?.email).not.toEqual('teste.usuario@gmail.com');
  });

  test('deve buscar um usuario de acordo com o email', async () => {
    const usuarioDTO = new UsuarioDTO({ nome: "TesteUsuario", email: "teste.usuario@gmail.com", senha: "123"});

    const isUsuarioRemovido = await removeUsuariosUseCase.execute(usuarioDTO);

    expect(isUsuarioRemovido);
  });

});
